import * as React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import ROUTES from "./configs/routes"
import PrivateRoute from "../src/components/commons/PrivateRoute/index";

import Navbar from './components/navbar'
import Home from './components/Home'
import Footer from './components/Footer'

import Login from './components/Login'
import Signup from './components/signup'
import Room from './components/room'

function App() {
  const token = localStorage.getItem('token')

  const [isLogin, setIsLogin] = React.useState(null)

  React.useEffect(() => {
    if (isLogin === null) {
      localStorage.removeItem('token')
    }

    setIsLogin(token)
  }, [token, isLogin])

  const logOut = () => {
    setIsLogin(null)
  }

  return (
    <Router>
      <Navbar  />
      <Switch>
        <Route path={ROUTES.ROOT} exact>
          <Home />
        </Route>
        <Route path={ROUTES.SIGNUP} exact>
          <Signup />
        </Route>
        <Route path={ROUTES.LOGIN} exact>
          <Login setIsLogin={setIsLogin}/>
        </Route>
        <PrivateRoute path={ROUTES.LOBBY} exact>
       <Room isLogin={isLogin} logOut={logOut}/>
      </PrivateRoute>
      <Route>
          <h1>404 Not Found</h1>
        </Route>
      </Switch>
      <Footer />
    </Router>
  )
}

export default App

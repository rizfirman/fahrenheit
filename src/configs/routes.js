const ROUTES = {
    ROOT: "/",
    HOMEPAGE: "/home",
    LOGIN: "/login",
    SIGNUP: "/signup",
    LOBBY: "/lobby",
  };
  
  export default ROUTES;
  
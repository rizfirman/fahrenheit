import axios from "axios";

class loginApi {
    static login = async (username, password) => {
      try {
        const res = await axios.post(
          `${process.env.REACT_APP_ROOT_URL}/user/login`,
          {
            username,
            password,
          }
        )
  
        return res.data
      } catch (error) {
        return error.response
      }
    }
  }
  
  export default loginApi
  
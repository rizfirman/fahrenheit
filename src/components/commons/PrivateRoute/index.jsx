import { Redirect } from "react-router-dom";
import ROUTES from "../../../configs/routes"

const PrivateRoute = ({ children }) => {
  if (!localStorage.getItem("token")) return <Redirect to={ROUTES.ROOT} />;

  return children;
};

export default PrivateRoute;

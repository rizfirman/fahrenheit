import * as React from 'react';
import { Link } from 'react-router-dom'


const Signup = () => {
    
    return (
        <>
        <section>
        <div className="signupcontent">
        <div id="about">
        <div className="row justify-content-lg-end me-auto">
        <h1 className="ml-xl-5 pb-lg-5">PLEASE SIGNUP!</h1> 
        <div className="form-group col-5 pt-lg-5 ">
                      <label htmlFor="username" className="text" >USERNAME</label>
                      <input type="text" className="form-control " id="username" name="username"
                       placeholder="Enter your username" required></input>
       
                      <label htmlFor="password" className="text" >PASSWORD</label>
                      <input type="password" className="form-control" id="password" name="password"
                       placeholder="Enter your password"  onKeyUp="dotDisplay()" required  ></input>
       
                      <label htmlFor="confirm_password" className="text">CONFIRM PASSWORD</label><span class="dot" id="dot"></span>
                      <span></span><input type="password" className="form-control" id="confirm_password" name="confirm_password"
                       placeholder="Enter your password again" onKeyUp="check()" required ></input>
                      
       
                      <label htmlFor="name" className="text">NAME</label>
                      <input type="text" className="form-control" name="name" id="name" placeholder="Enter your real name"></input>
       
                      <label htmlFor="city" className="text" >CITY</label>
                      <input type="text" className="form-control" name="city" id="city" placeholder="Enter your hometown" ></input>
       
                      <label htmlFor="gender" className="text" >GENDER</label><br></br>
                      <input type="radio" name="gender" id="male" value="m" className="mr-3" ></input><label for="male" className="text mr-5" >Male</label>
                      <input type="radio" name="gender" id="female" value="f" className="mr-3"></input><label for="female" className="text">Female</label>
                  </div>
                  
        </div>
        <div className="float-right">
        <h6 className="text ">Already has an account? Login <Link className="text" to="/login">here</Link></h6>
        <button type="submit" className="btn btn-primary mr-5" >SIGNUP</button> <br/><br/>
        
        </div>
       </div>
       </div>
       </section>
    
   
           
   
   
         
   
   
   
    </>

    ) 
    
}

export default Signup
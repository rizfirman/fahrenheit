import * as React from 'react'
import { Link } from 'react-router-dom'
 
import rockpaperstrategy from '../../assets/rockpaperstrategy-1600.jpg'
import user1 from '../../assets/user1.png'
import user2 from '../../assets/user2.png'
import user3 from '../../assets/user3.png'
import twitter from '../../assets/tw.png'





const Home = () => {
  return (
    <>
        <div className="masthead text-center text-white">
        <div className="masthead-content">
          <div className="container">
            <h1 className="masthead-heading mb-0">PLAY TRADITIONAL GAME</h1>
            <h2 className="masthead-subheading mb-0">Experience new traditional game play</h2>
            <Link className="btn btn-primary btn-xl text-uppercase" to="/login">Play now</Link>
          </div>
        </div>
        </div>

        
          <div className="halaman2">
                <div className="container">
                    <div className="col-lg-10 mb-5">
                        <div className="p-9" >
                        <h4>What's so special?</h4>
                        <h3 className="display-4">THE GAME</h3>
                        </div>
                    </div>
                </div>

                <div className="col-lg-8 order-11 ml-auto">
                    <div className=" bd-example">
                        <div id="carouselExample" class="carousel slide" data-ride="carousel">
                                <ol className="carousel-indicators">
                                    <li data-target="#carouselExample" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExample" data-slide-to="1"></li>
                                    <li data-target="#carouselExample" data-slide-to="2"></li>
                                    <li data-target="#carouselExample" data-slide-to="3"></li>
                                    <li data-target="#carouselExample" data-slide-to="4"></li>
                                </ol>
  
                        <div className="carousel-inner">
                        <div className="carousel-item active">
                            <img src={rockpaperstrategy} className="d-block w-100" alt="..."/>
                        </div>
                        </div>
  
                        <div className="carousel-item">
                            <img src="" class="d-block w-100" alt="..."/>
                        <div className="carousel-caption d-none d-md-block">
                        </div>
                        </div>
  
                        <div className="carousel-item">
                            <img src="" className="d-block w-100" alt="..."/>
                        <div className="carousel-caption d-none d-md-block">   
                        </div>
                        </div>
  
                        <a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
                            <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span className="sr-only">Previous</span>
                        </a>
                                            
                        <a className="carousel-control-next" href="#carouselExample" role="button" data-slide="next">
                            <span className="carousel-control-next-icon" aria-hidden="true"></span>
                            <span className="sr-only">Next</span>
                        </a>
        </div>
        </div>
        </div>
        </div>

        <div className="halaman3">
      

              <div className="container mt-5 mb-5 ml-auto">
                <div className="row">
                  <div className="col-md-6 offset-md-7">
                    <h2>What's so special?</h2>
                    <h1> THE FEATURES</h1>
                    <ul className="timeline">
                      <li>
                        <a target="" href="">TRADITIONAL GAMES</a>
                        <a href="#" className="float-right"></a>
                        <p> If you miss your childhood, we provide many traditional games here</p>
                      </li>
                      <li>
                        <a href="#">GAME SUIT</a>
                        <a href="#" className="float-right"></a>
                        <p></p>
                      </li>
                      <li>
                        <a href="#">TBD</a>
                        <a href="#" className="float-right"></a>
                        <p></p>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>  
    </div>

      <div className="halaman4">
      <div className="container" >
        <h4 className="text-center mt-4" >Can My Computer Run this game?</h4>
        <br></br>
        <br></br>
        <br></br>
                <h1>SYSTEM REQUIREMENTS</h1>
        <br></br>
        <br></br>
        <br></br>             
                <table className="table-sm table-bordered" width="500px">
                    <tbody>
                            <tr>
                                <td className="bordered">
                                    <h1> OS: </h1>
                                    <p>Windows 7 64-bit only (no OSX support at this time)</p>
                                </td>

                                <td>
                                    <h1>PROCESSOR:</h1>
                                    <p>Intel Core 2 Duo @ 2.4 GHz or AMD Athlon X2 @ 2.8 GHz</p>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <h1>MEMORY:</h1>
                                    <p>4 GB RAM</p>
                                </td>
                  
                                <td>
                                    <h1 >STORAGE:</h1>
                                    <p>8 GB available space</p>

                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <h1> GRAPHIC:</h1>
                                    <p>NVIDIA GeForce GTX 660 2GB or AMD Radeon HD 7850 2 GB DirectX11 (Shader Model 5)</p>
                                </td>
                            </tr>
                    </tbody>
                </table>
            <br></br>
            <br></br>
            <br></br>
        </div>
    </div>
       
    <div className="halaman5">
        <div className="container">

            <div className="row">

                <div className="col-md-6 col-12 my-auto py-2">
                    <h2>TOP SCORES</h2>
                    <p>This top score from various games provide on this platform</p>
                    <button className="btn btn-primary px-5 mt-4">SEE MORE</button>
                </div>

                <div className="col-md-6 col-12 my-auto">
                    <div className="card ml-auto mb-3">
                        <div className="card-body p-2">

                            <div className="row">
                                <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                    <img className="img-user-topscore" src={user1} width="50" height="50" alt="user one"/>
                                </div>

                                <div className="col-7 col-sm-8 col-md-6 col-lg-8">
                                    <p className="top-score-name">Rizki Firmansyah</p>
                                    <p className="grey-text">PC Gamer</p>
                                </div>

                                <div className="col-2 col-sm-2 col-md-3 col-lg-2">
                                    <img src={twitter} alt="twitter logo" height="30" width="30"/>
                                </div>

                                <div className="col-12 mt-2">
                                    <p><q>One of my gaming highlights of the year</q></p>
                                    <p className="grey-text">October 12, 2018</p>
                                </div>
                        </div>
                    </div>
                </div>

                <div className="card mr-auto mb-3">
                    <div className="card-body p-2">

                        <div className="row">

                            <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                <img className="img-user-topscore" src={user2} width="50" height="50" alt="user one"/>
                            </div>

                            <div className="col-7 col-sm-8 col-md-6 col-lg-8">
                                <p className="top-score-name">Rizki Firmansyah</p>
                                <p className="grey-text">PC Gamer</p>
                            </div>

                            <div className="col-2 col-sm-2 col-md-3 col-lg-2">
                                <img src={twitter} alt="twitter logo" height="30" width="30"/>
                            </div>

                            <div className="col-12 mt-2">
                                <p><q>One of my gaming highlights of the year</q></p>
                                <p className="grey-text">October 12, 2018</p>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div className="card ml-auto mb-3">
                    <div className="card-body p-2">

                        <div className="row">

                            <div className="col-3 col-sm-2 col-md-3 col-lg-2">
                                <img className="img-user-topscore" src={user3} width="50" height="50" alt="user one"/>
                            </div>

                            <div className="col-7 col-sm-8 col-md-6 col-lg-8">
                                <p className="top-score-name">Rizki Firmansyah</p>
                                <p className="grey-text">PC Gamer</p>
                            </div>

                            <div className="col-2 col-sm-2 col-md-3 col-lg-2">
                                <img src={twitter} alt="twitter logo" height="30" width="30"/>
                            </div>

                            <div className="col-12 mt-2">
                                <p><q>One of my gaming highlights of the year</q></p>
                                <p className="grey-text">October 12, 2018</p>
                            </div>

                        </div>   
                    </div>
                </div>
          </div>
          
        </div>
      </div>
    </div>

    <div className="halaman6">
        <div id="about">
            <div className="container">
                <div className="row justify-content-end">

                    <div className="col-12 col-sm-12 col-md-6 col-lg-6 pt-5 mb-5">
                        <p>Want to stay in touch?</p>
                        <h2>NEWSLETTER SUBSCRIBE</h2>
                        <p>In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We don’t spam.</p>  
                        
                        <form>
                            <div className="row">
                                <div className="col-12 col-sm-6 mb-3">
                                    <input type="email" className="form-control bg-dark text-white" placeholder="yourmail@example.com"></input>
                                </div>

                                <div className="col-12 col-sm-6">
                                    <a href="#" className="btn btn-primary" id="subscribebtn">SUBSCRIBE NOW</a>
                                </div>
                            </div>
                  
                        </form>

                    </div> 
                </div>
            </div>             
        </div>
        </div>
    </>
  )
}

export default Home

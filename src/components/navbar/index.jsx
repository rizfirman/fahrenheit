import * as React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => {
    return (
      <nav className="navbar navbar-expand-lg navbar-dark navbar-custom p-3 fixed-top">
      <a className="navbar-brand mx-5" href="#">LOGO</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse mx-5" id="navbarNavDropdown">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item mr-4">
            <Link className="nav-link nav-link-text" to='/'>HOME
          </Link>
          </li>
          <li className="nav-item mr-4">
            <a className="nav-link nav-link-text" href="#features">GAME FEATURES</a>
          </li>
          <li className="nav-item mr-4">
            <a className="nav-link nav-link-text" href="#top-scores">QUOTES</a>
          </li>
          <li className="nav-item">
            <a className="nav-link nav-link-text" href="#about">ABOUT</a>
          </li>
        </ul>

        <ul className="navbar-nav ml-auto">
          <li className="nav-item mr-4">
            <Link className="nav-link nav-link-text" to="/signup">SIGN UP</Link>
          </li>
        
          <li className="nav-item">
            <Link className="nav-link js-scroll-trigger" to='/login'>LOG IN</Link>
          </li>
        </ul>

      </div>
    </nav>
    )
}

export default Navbar
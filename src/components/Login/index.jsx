import * as React from 'react';
import { Link, useHistory } from 'react-router-dom';
import ROUTES from "../../configs/routes";
import loginApi from "../../services/login"

const Login = ({ setIsLogin }) => {
  const history = useHistory()
  const [username, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [isError, setIsError] = React.useState('')

  const onSubmit = async () => {
    try {
      const res = await loginApi.login(username, password)

      if (res.status === 400) {
        throw new Error('can`t login')
      }

      localStorage.setItem('token', res.token)
      setIsLogin(localStorage.getItem('token'))
      history.push(ROUTES.LOBBY)
    } catch (error) {
      setIsError(error.message)
    }
  }
    return (
        
        <section>
            <div className="Logincontent">
                
                <div className="row login-form justify-content-lg-end mr-auto">
                  <h1 className=" ml-xl-5 pb-lg-5">PLEASE, LOGIN FIRST!</h1> 
                <div className="form-group col-5 pt-lg-5 ">
                  <label htmlFor="email" className="text" >EMAIL</label>
                  <input 
                  type="email" 
                  className={`form-control ${isError ? "is-invalid" : ""}`} 
                  id="email" 
                  name="email"
                  placeholder="Enter your Email" 
                  onChange={(e) => {
                    setIsError(false);
                    setEmail(e.target.value);
                  }}
                  required
                  >
                  </input>

                  <label htmlFor="password" className="text pt-lg-5" >PASSWORD</label>
                  <input 
                  type="text password"
                  className={`form-control ${isError ? "is-invalid" : ""}`} 
                  id="password" 
                  name="password"
                  value={password}
                  placeholder="Enter your password"  
                  onChange={(e) => {
                    setIsError(false);
                    setPassword(e.target.value);
                  }}
                  required>
                  </input>
                
                </div>
                </div>
                <div className="float-right">
                <h6 className=" text mr-3">Don't have any account? Please <Link className="text" to="/signup">signup</Link></h6>
                <button 
                type="submit" 
                className="reminder row login btn btn-primary mr-auto"
                onClick={onSubmit}
                >SUBMIT</button> 
                </div> 
                  
                 
              </div>
              </section>
      
    )
}

export default Login


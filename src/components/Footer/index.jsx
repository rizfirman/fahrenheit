import * as React from 'react';
import twitter from '../../assets/tw.png';
import facebook from '../../assets/facebook-logo-2019.png';
import twitch from '../../assets/twitch-512.webp';
import youtube from '../../assets/youtubelogo.png';

const Footer = () => {
  return (
      <>
      
    
    <div className="container-fluid px-5 footer-info ">

        <div className="row justify-content-end">
            <ul className="nav">
                <li className="nav-item">
                <a className="nav-link active" href="#">MAIN</a>
                </li>

                <li className="nav-item">
                <a className="nav-link" href="#about">ABOUT</a>
                </li>

                <li className="nav-item">
                <a className="nav-link" href="#features">GAME FEATURES</a>
                </li>

                <li className="nav-item">
                <a className="nav-link" href="#system-req">SYSTEM REQUIREMENTS</a>
                </li>

                <li className="nav-item mr-auto">
                <a className="nav-link" href="#top-scores">QUOTES</a>
                </li>
                <div className=" smlogo d-inline-flex p-2">
                <li className="nav-item">
                <a className="nav-link-fb ml-lg-2" href="#"> <img src={facebook} height="28" width="28" alt="facebook" title="facebook"/></a>
                </li>

                <li className="nav-item">
                <a className="nav-link-tw ml-lg-3" href="#"> <img src={twitter} alt="twitter logo" height="30" width="30" title="twitter"/></a>
                </li>

                <li className="nav-item ">
                <a className="nav-link-yt ml-lg-2" href="#"> <img src={youtube} width="40" height="30" alt="youtube" title="youtube"/></a>
                </li>

                <li className="nav-item">
                <a className="nav-link-tch ml-lg-2" href="#"> <img src={twitch} width="30" height="30" alt="twitch" title="twitch"/></a>
                </li>
                </div>
            </ul>
        </div>

        <hr className="my-3"></hr>

            <div className="row">
                <div className="col-12 col-sm-6 col-md-6">
                    <p>&copy;2020 Your Games inc. All Rights Reserved</p>
                </div>

                <div className="col-12 col-sm-6 col-md-6">
                    <ul className="nav justify-content-end">

                        <li className="nav-item">
                        <a className="nav-link bottom-nav" href="#">PRIVACY POLICY</a>
                        </li>

                        <li className="nav-item">
                        <a className="nav-link bottom-nav" href="#">TERM OF SERVICE</a>
                        </li>

                        <li className="nav-item">
                        <a className="nav-link bottom-nav" href="#">CODE OF CONDUCT</a>
                        </li>
                    </ul>
                </div>

            </div>
    </div>
    

     </>  
  )
}

export default Footer

import * as React from 'react';
import ROUTES from "../../configs/routes";
import { Link } from 'react-router-dom'

const Room = () => {
    
    return (
            <>
            <section>
            <div className="roomcontent">
                
                <div className=" d-flex justify-content-lg-center mb-lg-5">
                <div className="d-grid gap-2 col-6 mx-auto">
                    <button id="create_room" className="btn  btn-primary mt-lg-5 ">Create Room</button>
                    <button id="join_room" className="btn btn-primary " onclick="join()">Join Room</button>
                    <button id="vs_computer" className="btn btn-primary">VS Computer</button>
                     
                    <button id="history" className="btn btn-primary  ">History</button>
                    <button id="edit" className="btn btn-primary">Edit</button>
                    <Link to={ROUTES.ROOT} className="btn btn-primary" >Logout</Link>
                   
                    
                </div>
                 
              </div>
              </div>
              </section>
        
            </>
    )
}

export default Room

